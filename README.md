# Node installation
Go to [nodejs.org][node] download and install. Once installed verify sucessfull installation by running below command.

```from prompt like cmd or sh
node --version or node -v //v9.2.1.16 meaning major: v9, minor:2, patch:1, bug: 16
npm --version  or npm -v
```
#### Other node command
* npm init
* npm install  with option --save, --save-dev 
* npm list
* npm link * //run from a module, Create a link in global module folder (user.../node_modules)
* npm link <module_name> //create a link from global module folder to local node_moduels
* npm publish  
* npm unpublish --force
**need an acc before publish or unpublish**
# Interactive Terminal
* Type `node` go get prompt. Here we can execute JavaScript.
* type .exit or press ctrl+c twoice to come out from node terminal
```JS
$node
>console.log('Hellow World');
>function(){
... console.log('function called');
... var a = 2+3; // any  thing
...} // Here triple dot represent continuation of function 
>
```
# Debug Node JS App
* Microsoft visual studio code editor comes with build in debugger by using launch.json
* If we are using ATOM then install **node-debugger** plugin and configre it.
```launch.json
{
    "type": "node",
    "request": "launch",
    "name": "Launch in WSL",
    "useWSL": true,
    "program": "${workspaceFolder}/hello.js"
}
```
* Remote debugging is also possilble by below configuration
``` launch.json Remote debugging
{
    "type": "node",
    "request": "attach",
    "name": "Attach to remote",
    "address": "TCP/IP address of process to be debugged",
    "port": "9229",
    "localRoot": "${workspaceFolder}",
    "remoteRoot": "C:\\Users\\John\\project\\server"
}
```
# More about Javascript
* Type of variable in JS
  * Basic Data type
    - number
    - string
    - boolan
  * Other data type
    * Objet
    * Array
    * function
  * Special values    
    * null
    * undefined
    * Infinity
    * NaN
    
**JSON holds the key and value in double quote**  

### Error object
 ```js
 try{
 new Error("Some message");
 }catch(e){
 console.log(e.message);
 }
 ```
 ### Node Globals
 * Node has global (can say same as window in browser).
 * console.log('global') //here console comes from 'global' var

### Array function
* var sqre = a => a*a; 
* var sum = (a,b) => a+b
* var diff = (a,b) => {return a-b}
* var obj = {key1:val, key2:val2 ...etc};
* for(idx in obj){console.log(idx+ '  '+ obj[idx]);}
* var arr = [1,2,3,3,4];
* for (value of arr){console.log(value);} //works only on array
# Asynchronous Programming
**use `self =this`; Reason `this` does not exist inside in async functions like setTimeout etc but `self` exist**
** We also also use `arrow function` to avoid this problem**

### process.nextTick(funciton)
Add the function at the head of eventque queue and it's called just after the current function.
### setImmidiate add the callback funciton in eventque behind whatever callbacks that are already in the event queue
**process.nextTick and setImmidiate both funciton are used to achieved the async functionality**

**Bacically callback func takes two argument first one is error and second one is data**

```js
callback(error, data){
}

```
### Support query and path param in your server

Use **var build in url =require('url')** to read req and query param 

```js
var url =require('url')
var parseUrl =url.parse(req.url, ture);
var path = parseUrl.pathname;
var query= parseUrl.query; 
```
### Support HTTP method in your server
user **var qs = require('querystring');** to parse the body
```post
if(request.method === "POST") {
    if (request.url === "/inbound") {
      var requestBody = '';
      request.on('data', function(data) {
        requestBody += data;
        if(requestBody.length > 7) {
          response.writeHead(413, 'Request Entity Too Large', {'Content-Type': 'text/html'});
          response.end('<!doctype html><html><head><title>413</title></head><body>413: Request Entity Too Large</body></html>');
        }
      });
      request.on('end', function() {
        var formData = qs.parse(requestBody);
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write('<!doctype html><html><head><title>response</title></head><body>');
        response.write('Thanks for the data!<br />User Name: '+formData.UserName);
        response.write('<br />Repository Name: '+formData.Repository);
        response.write('<br />Branch: '+formData.Branch);
        response.end('</body></html>');
      });
    } else {
      response.writeHead(404, 'Resource Not Found', {'Content-Type': 'text/html'});
      response.end('<!doctype html><html><head><title>404</title></head><body>404: Resource Not Found</body></html>');
    }
}
```
# Node Module
**In general, the rule of thumb is:**
* If you’re installing something that you want to use in your program, using require('whatever'), then install it locally, at the root of your project.
* If you’re installing something that you want to use in your shell, on the command line or something, install it globally, so that its binaries end up in your PATH environment variable

**Every file in node js is module and return module.exports by default.**
**ie**  when we write 
```node.js
 var greet = function () {
         console.log('Hello World');
      };
     module.exports = greet;
```
**is converted** in
```
(function (exports, require, module, __filename, __dirname) { //add by node
  var export  = module.export;
      var greet = function () {
         console.log('Hello World');
      };
      module.exports = greet; //exports =greet;
})(); //add by node
return module.exports;
```
### Node module path.
* First it search for the file (not npm module) at given path.
* if not found then it search the module in local (project's) node_modules.
##### require('modulename') vs require('modulename.js')
 * if extention is given then it directly search the file only
 * if extention is not given then first it search the file and if file is not found then it reads package.json inside `node_modules/modulename` and load the file with respect to main key
##### What if two modules of diff version are loaded.
consider module1 loads module3v1 and module2 loads module3v2 then both modules use their own loaded version of module3
##### Cyclic dependancy .... No problem
say module a.js loads b.js and b.js load a.js then no problem of cyclic dependancy both refer to each other
##### NPM Link
```node
modulea >npm link . // create a link of current module in global node_modules
moduleb> npm link <module_name> // create a link of given module into local node_modules
and then it can be used in the project
```

1. **use npm install async and require('async') to perform some async operations**
1. **use npm install fs and require('fs') to perform  file/stream operations**
1. **use var rs = fs.readStream('filename') rs.pipe(res) to write all content to res with tuned w/r opertion of netwrok and stream**



























[node]: <https://nodejs.org/en/>
